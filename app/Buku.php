<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Schema;

class Buku extends Model
{
    protected $table = 'programs';
    public $timestamps = false;
    protected $casts = [
        'nama' => 'array',
    ];

    public function columnName(){
        $column = Schema::getColumnListing($this->getTable());
        return $column;
    }
}
