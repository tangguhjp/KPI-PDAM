<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

use Maatwebsite\Excel\Excel;
use App\Buku;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\Storage;
use DB;
class ContactController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function getContact()
    {
        return view('registration.registration');
    }

    public function postContact(Request $request)
    {
        $code = $request->input('CaptchaCode');
        $isHuman = captcha_validate($code);

        if ($isHuman) {
            return "LOGIN";
            return redirect()->action('ContactController@index');
            // Captcha validation passed
            // TODO: continue with form processing, knowing the submission was made by a human
            return redirect()
                ->back()
                ->with('status', 'CAPTCHA validation passed, human visitor confirmed!');
        }

        // Captcha validation failed, return an error message
        return redirect()
            ->back()
            ->withErrors(['CaptchaCode' => 'CAPTCHA validation failed, please try again.']);
    }

    public function testexcel(Request $request)
    {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $path = 'app/doc/';
        $data = Buku::find(1);

        if ($request->hasFile('file')){
            $file = $request->file('file');
            Storage::put('doc/1.'.$file->getClientOriginalExtension(), file_get_contents($request->file('file')->getRealPath())); //storage/app/...

            $data['nama']  = $path.'1.'.$file->getClientOriginalExtension();
            $data->save();
            return $data;
            $spreadsheet = $reader->load($file);
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A10', 'Hello Hello !');

            $writer = new Xlsx($spreadsheet);

            Storage::put('tes6.'.$file->getClientOriginalExtension(), file_get_contents($request->file('file')->getRealPath())); //storage/app/...
            $user = DB::table('KOTA')->insertGetId($path);

//            $sheet->getCell('A3'); //getvalue from a cell
//            return storage_path('app');

        }else{
            return "TIDAK FILE";
        }
//        $spreadsheet = new Spreadsheet();

        $spreadsheet = $reader->load("New.xlsx");
        $sheet = $spreadsheet->getActiveSheet();
        $cells = 'A1';
//        if ($id==1){
//            $cells = 'A3';
//        }
        $sheet->setCellValue($cells, 'Hello Edwin !');

        $writer = new Xlsx($spreadsheet);
        if ($writer->save('../storage/Newdjhdehdh.xlsx')){
            dd("sukses");
        }
        return "BERHASIL";
    }

    public function downloadFile(){
        $data = Buku::find(1);
        $path= storage_path($data->nama);
        return response()->download($path);
    }

    public function dbconn(){
        $tes =  Buku::all()->first();
        $ar = $tes->columnName();
        return $ar;
//        $data['nama'] = 'Tanggo';
        $user = Buku::find(1);
        $nama = $user->nama;
        $nama['target'] = 200;
        $nama['path'] = 'deddr';
        $nama['approved'] = 0;
        $user->nama = $nama;
        $user->save();
        if ($user->nama['approved'] == 0){
            return 'NOL';
        }else{
            return $user->nama['approved'];
        }
    }
}
