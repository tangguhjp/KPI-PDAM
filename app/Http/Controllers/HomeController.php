<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Passwords\PasswordResetServiceProvider;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Program;
use App\User;
use App\Target;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::guest()){
            return Redirect::to('auth/login');
        }else{
            return view('menu.index');
        }
    }

    public function login(Request $request){
        $code = $request->input('CaptchaCode');
        $isHuman = captcha_validate($code);

        if ($isHuman) {
            return redirect()->route('home');
            return redirect()->action('ContactController@index');
            // Captcha validation passed
            // TODO: continue with form processing, knowing the submission was made by a human
            return redirect()
                ->back()
                ->with('status', 'CAPTCHA validation passed, human visitor confirmed!');
        }

        // Captcha validation failed, return an error message
        return redirect()
            ->back()
            ->withErrors(['CaptchaCode' => 'CAPTCHA validation failed, please try again.']);
    }

    public function home(){
        if (Auth::guest()){
            return Redirect::to('auth/login');
        }else{
            return view('menu.index');
        }
    }

    public function responsibility()
    {
        $data = [];
        $program = Program::where('responsible', Auth::user()->id)->get();
        foreach ($program as $p){
            $p->responsible =  User::where('id', $p->responsible)->get()->pluck('name')->first();
            $p->target_type =  Target::where('id', $p->target_type)->get()->pluck('description')->first();
            array_push($data,$p);
        }

        return view('menu.responsibility')->with(compact('data'));
    }

    public function approve()
    {
        $user = User::where('manager', Auth::user()->id)->get();
        $data=[];
        foreach ($user as $u){
            $program = Program::where('responsible', $u->id)->get();
            foreach ($program as $p){
                $p->responsible =  User::where('id', $p->responsible)->get()->pluck('name')->first();
                $p->target_type =  Target::where('id', $p->target_type)->get()->pluck('description')->first();
                array_push($data,$p);
            }
        }
        return view('menu.responsibility')->with(compact('data'));
    }

    public function inputProgram(){
        $data = [];

        $id = Auth::user()->id;
        $div_name = User::where('manager', $id)->orderBy('name')->get()->pluck('name')->all();
        $target_type = Target::orderBy('description')->get()->pluck('description')->all();

        $data['division'] = $div_name;
        $data['type'] = $target_type;

        return view('menu.input-program')->with(compact('data'));
    }

    private function approval($data){
        $role = Auth::user()->role;
        $month = $data['month'];

        $program = Program::find($data['programId']);
        $monthData = $program[$month];
        if ($role == 4){
            $monthData['approved'] = 2;
        }else{
            $monthData['approved'] = 1;
        }

        $program[$month] = $monthData;
        if ($program->save()){
            return true;
        }else{
            return false;
        }
    }
    public function testing(Request $request){
        if ($this->approval($request->all())){
            $data = $request->all();
            $data['programId'] = Program::find($data['programId']);
            return view('menu.popup')->with(compact('data'));
        }else{
            return "NOT APPROVED";
        }
    }
}
