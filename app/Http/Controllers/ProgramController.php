<?php

namespace App\Http\Controllers;

use App\Target;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Program;
use Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Collection;


class ProgramController extends Controller
{
    public function input(Request $request){
        $data = $request->all();
        if (Program::insert($data)){
            return $data;
        }else{
            return "salah";
        }
    }

    //create directory check if exist or not for excel file
    private function createDir($dirname){
        if (file_exists(storage_path().'app/doc/'.$dirname)) {
            return true;
        }
        Storage::disk('local')->makeDirectory('doc/'.$dirname);
        return false;
    }

    public function uploadFileRealisation(Request $request){
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $programId = $request->programId;
        $month = $request->month;
        $path = 'app/doc/'.$programId.'/'.$month; //path in storage directory

        $fileExcel = $request->file('fileExcel');
        $date = new DateTime();
        $randomName = Carbon::now();

        $program = Program::find($programId);

        if ($request->hasFile('fileExcel')){
            $this->createDir($programId); //check program Id directory
            $this->createDir($programId.'/'.$month); //check month directory in program Id

            //upload excel file to storage
            Storage::put('doc/'.$programId.'/'.$month.'/'.$randomName.'.'
                .$fileExcel->getClientOriginalExtension(),
                file_get_contents($fileExcel->getRealPath())); //storage/app/...

            $spreadsheet = $reader->load($fileExcel);
            $sheet = $spreadsheet->getActiveSheet();

            $coordinateCell = $this->getCoordinateCell($month);
            (string)$realisation = $sheet->getCell($coordinateCell)->getValue();

            $m['target'] = $realisation;
            $m['path'] = $path.'/'.$randomName.'.'.$fileExcel->getClientOriginalExtension();
            $m['approved'] = 0;

            $program[$month] = $m;

            $data['label'] = $request->label;
            $data['month'] = $month;
            $data['programId'] = Program::find($request->programId);
            if ($program->save()){
                return view('menu.popup')->with(compact('data')); //redirect to this page
            }else{
                return 'Saving Error';
            }
        }else{
            return "File Excel Not Founds";
        }

        //condition cell when different month (NOT YET)
//        $spreadsheet = new Spreadsheet();

        $spreadsheet = $reader->load("New.xlsx");
        $sheet = $spreadsheet->getActiveSheet();
        $cells = 'A1';
//        if ($id==1){
//            $cells = 'A3';
//        }
        $sheet->setCellValue($cells, 'Hello Edwin !');

        $writer = new Xlsx($spreadsheet);
        if ($writer->save('../storage/Newdjhdehdh.xlsx')){
            dd("sukses");
        }
        return "BERHASIL";
    }

    private function getCoordinateCell($month){
        $collection = collect([
            'jan' => 'A1',
            'feb' => 'A2',
            'mar' => 'A3',
            'apr' => 'A4',
            'may' => 'A5',
            'june' => 'A6',
            'july' => 'A7',
            'aug' => 'A8',
            'sept' => 'A9',
            'oct' => 'A10',
            'nov' => 'A11',
            'dec' => 'A12',
        ]);

        $value = $collection->get($month);

        return $value;
    }

    public function popUpInput(Request $request){
        $data = $request->all();
        $data['programId'] = Program::find($request->programId);
        return view('menu.popup')->with(compact('data'));
    }

    //Download Excel File
    public function downloadFile(){
        $data = Program::find(1);
        $path= storage_path($data->jan['path']);
        return response()->download($path);
    }

    private function encodeMonth($data){
        $monthId = ['jan','feb','mar','apr','may','june','july','aug','sept','oct', 'nov', 'dec'];
        $monthData = ['target' => '', 'path' => '', 'approved' => ''];
        for ($i = 0; $i<12; $i++){
            $data[$monthId[$i]] = json_encode($monthData);
        }
        return $data;
    }

    private function decodeMonth($data){
        $monthId = ['jan','feb','mar','apr','may','june','july','aug','sept','oct', 'nov', 'dec'];
        $monthData = ['target' => '', 'path' => '', 'approved' => ''];
        for ($i = 0; $i<12; $i++){
            $data[$monthId[$i]] = json_decode($data[$monthId[$i]]);
        }
        return $data;
    }
    public function insertProgram(Request $request){
        $data = $request->all();
        unset($data['_token']);
        $responsible = User::where('name', $request->responsible)->get()->first();
        $target_type = Target::where('description', $request->target_type)->get()->first();
        $data['responsible'] = $responsible->id;
        $data['target_type'] = $target_type->id;

        $data = $this->encodeMonth($data); //

        if (Program::insert($data)){
            return $data;
        }else{
            return "salah";
        }
        return $data;
    }

    private function approval($data){
        $role = Auth::user()->role;
        $month = $data['month'];

        $program = Program::find($data['programId']);
        $monthData = $program[$month];
        if ($role == 4){
            $monthData['approved'] = 2;
        }else{
            $monthData['approved'] = 1;
        }

        $program[$month] = $monthData;
        if ($program->save()){
            return true;
        }else{
            return false;
        }
    }
    public function accept(Request $request){
        if ($this->approval($request->all())){
            $data = $request->all();
            $data['programId'] = Program::find($data['programId']);
            return view('menu.popup')->with(compact('data'));
        }else{
            return "NOT APPROVED";
        }
    }
}