<?php

//
//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', [
    'as' => 'index', 'uses' => 'HomeController@index'
]);

Route::post('/', ['as' => 'login', 'uses' => 'HomeController@login']); //login user

Route::get('/home',  ['as'=> 'home', 'uses' => 'HomeController@home']); //home menu

Route::get('/responsibility', [
    'as' => 'responsibility', 'uses' => 'HomeController@responsibility'
]);

Route::get('/approve', [
    'as' => 'approve', 'uses' => 'HomeController@approve'
]);

Route::get('/input-program', [
    'as' => 'input-program', 'uses' => 'HomeController@inputProgram'
]);

Route::post('/insert-program', [
    'as' => 'insert-program', 'uses' => 'ProgramController@insertProgram'
]);

Route::post('input','ProgramController@input'); //input program

Route::post('/popUpInput', 'ProgramController@popUpInput'); //pop up windows
Route::get('/popUpInput', 'ProgramController@popUpInput')->name('popUpInput'); //pop up windows
//Route::post('/inputFile','ProgramController@uploadFileRealisation'); //input file excel realisasi tiap bulan
Route::post('/inputFile', [
    'as' => 'inputFile', 'uses' => 'ProgramController@uploadFileRealisation'
]);

Route::get('download', 'ProgramController@downloadFile');
Route::post('accept', [
    'as' => 'accept', 'uses' => 'ProgramController@accept'
]);

Route::get('register', [
    'as' => 'register', 'uses' => 'SimpleauthController@register'
]);

Route::get('data', 'TestController@test');


//Test Captcha
Route::get('example', 'ExampleController@getExample');
Route::post('example', 'ExampleController@postExample');

Route::get('contact', 'ContactController@getContact');
Route::post('contact', 'ContactController@postContact');

//Route::get('home', 'ContactController@index');

Route::post('test', 'ContactController@testexcel');
Route::get('download-file', 'ContactController@dbconn');

//AUTH
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'UserController@login');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::post('/testing', [
    'as' => 'testing', 'uses' => 'HomeController@testing'
]);

?>
