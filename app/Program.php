<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Schema;

class Program extends Model
{
    protected $table = 'programs';
    public $timestamps = false;
    protected $casts = [
        'jan' => 'array',
        'feb' => 'array',
        'mar' => 'array',
        'apr' => 'array',
        'may' => 'array',
        'june' => 'array',
        'july' => 'array',
        'aug' => 'array',
        'sept' => 'array',
        'oct' => 'array',
        'nov' => 'array',
        'dec' => 'array',
    ];

    public function columnName(){
        $column = Schema::getColumnListing($this->getTable());
        return $column;
    }
}
