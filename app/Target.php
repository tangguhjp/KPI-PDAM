<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Schema;

class Target extends Model
{
    protected $table = 'target_type';
    public $timestamps = false;
}
