<?php

return [
    'oracle' => [
        'driver'   => 'oracle',
        'tns'      => env('DB_TNS', ''),
        'host'     => env('DB_HOST', '127.0.0.1'),
        'port'     => env('DB_PORT', '1521'),
        'database' => env('DB_DATABASE', 'EX'),
        'username' => env('DB_USERNAME', 'TESTER'),
        'password' => env('DB_PASSWORD', 'tester'),
        'charset'  => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'   => env('DB_PREFIX', ''),
    ],
];
