<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('target_type');
        Schema::create('target_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->unique();
            $table->string('description')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('target_type', function (Blueprint $table) {
            Schema::drop('target_type');
        });
    }
}
