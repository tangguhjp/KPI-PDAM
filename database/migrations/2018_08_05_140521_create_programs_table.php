<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('programs');
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('responsible'); //penanggungjawab
            $table->string('goal'); //sasaran
            $table->string('indicator');
            $table->string('measurement'); //pengukuran
            $table->string('unit'); //satuan
            $table->unsignedInteger('target_type');
            $table->string('target');
            $table->integer('period');
            $table->string('jan')->nullable();
            $table->string('feb')->nullable();
            $table->string('mar')->nullable();
            $table->string('apr')->nullable();
            $table->string('may')->nullable();
            $table->string('june')->nullable();
            $table->string('july')->nullable();
            $table->string('aug')->nullable();
            $table->string('sept')->nullable();
            $table->string('oct')->nullable();
            $table->string('nov')->nullable();
            $table->string('dec')->nullable();

            $table->foreign('responsible')->references('id')->on('USERS');
            $table->foreign('target_type')->references('id')->on('target_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs', function (Blueprint $table) {
            Schema::drop('programs');
        });
    }
}
