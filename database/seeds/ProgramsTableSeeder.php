<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            'responsible' => 3,
            'goal'=> 'goal program',
            'indicator' => 'lorem ipsum',
            'measurement' => 'panjang pipa',
            'unit'=> '%',
            'target_type'=> 1,
            'target'=> '100',
            'period'=> 2018,
            'jan'=> '90',
            'feb'=> '70',
            'mar'=> '100',
            'apr'=> '100',
            'may'=> '100',
            'june'=> '100',
            'july'=> '100',
            'aug'=> '100',
            'sept'=> '100',
            'oct'=> '100',
            'nov'=> '100',
            'dec'=> '100',
        ]);
    }
}
