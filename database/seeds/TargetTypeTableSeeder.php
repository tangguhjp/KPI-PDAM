<?php

use Illuminate\Database\Seeder;

class TargetTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('target_type')->insert([
            ['level' => 0,'description' => 'bulanan',],
            ['level' => 1,'description' => 'triwulan',],
            ['level' => 2,'description' => 'tahunan',],
        ]);
    }
}
