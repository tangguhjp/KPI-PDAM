<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_role')->insert([
            ['level' => 0,'description' => 'supervisor',],
            ['level' => 1,'description' => 'manager',],
            ['level' => 2,'description' => 'manager senior',],
            ['level' => 3,'description' => 'admin kkp',],
        ]);
    }
}
