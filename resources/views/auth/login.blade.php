@extends('master')
@section('content')
    <div id="login">
        <div class="container">
            <div >

            </div>
            <div class="row" style="margin-top: 7% ">
                    <div class="col-md-1 col-icon-login">
                        <img id="icon-login" src="{{asset('img/logo.png')}}" alt="blablabla">
                    </div>
                    <div class="col-md-8 col-header-login" >
                        <h1>KEY PERFORMANCE INDICATOR</h1>
                    </div>
            </div>
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" action="/auth/login" method="POST">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="username" class="label-form-login text-info">Username :</label><br>
                                <input type="text" name="username" value="{{old('username')}}" id="username" class="form-control" required >
                            </div>
                            <div class="form-group" >
                                <label for="password" class="label-form-login text-info">Password :</label><br>
                                <input type="password" name="password" id="password" class="form-control" required>
                            </div>
                            <div>
                                {!! captcha_image_html('ExampleCaptcha') !!}
                                <label class="label-form-login text-info">Retype the characters from the picture :</label>
                                <input type="text" id="CaptchaCode" class="form-control" name="CaptchaCode" required>
                            </div>
                            <br>
                            <div>
                                <button type="submit" class="btn btn-primary btn-login">LOGIN</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop