@extends('menu.menu-master')
@section('content')

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>Input KPI</title>
        </head>
        <body>
            <?php
                $division = $data['division'];
                $target_type = $data['type'];
            ?>
            <div class="container">
                <form id="program-input-form" action="{{route('insert-program')}}" method="post">
                    {!! csrf_field() !!}
                    <h2 class="text-center">Masukkan Program KPI</h2><br>
                    <div class="form-group row">
                        <label for="responsibility" class="col-3 col-form-label">Penanggung Jawab</label>
                        <select class="form-control col-9" id="responsibility" name="responsible">
                            @for($i = 0; $i < sizeof($division);$i++)
                                <option>{{$division[$i]}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="tujuan" class="col-3 col-form-label">Tujuan</label>
                        <textarea id="tujuan" class="form-control col-9" rows="2" name="goal" form="program-input-form"></textarea>
                    </div>
                    <div class="form-group row">
                        <label for="indikator" class="col-3 col-form-label">Indikator</label>
                        <textarea id="indikator" class="form-control col-9" rows="2" name="indicato" form="program-input-form"></textarea>
                    </div>
                    <div class="form-group row">
                        <label for="pengukuran" class="col-3 col-form-label">Pengukuran</label>
                        <textarea id="pengukuran" class="form-control col-9" rows="2" name="measurement" form="program-input-form"></textarea>
                    </div>
                    <div class="form-group row">
                        <label for="satuan" class="col-3 col-form-label">Satuan</label>
                        <input id="satuan" class="form-control col-9" type="text" name="unit">
                    </div>
                    <div class="form-group row">
                        <label for="tipe-target" class="col-3 col-form-label">Tipe Target</label>
                        <select class="form-control col-9" id="tipe-target" name="target_type">
                            @for($i = 0; $i < sizeof($target_type);$i++)
                                <option>{{$target_type[$i]}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="target" class="col-3 col-form-label">Target</label>
                        <input id="target" class="form-control col-9" type="text" name="target">
                    </div>
                    <div class="form-group row">
                        <label for="periode" class="col-3 col-form-label">Periode</label>
                        <input id="periode" class="date-own form-control col-9" type="text" name="period">
                    </div>
                    <div class="form-group row">
                        <input id="input" class="btn btn-primary col-12" type="submit">
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                $('.date-own').datepicker({
                    minViewMode: 2,
                    format: 'yyyy'
                });
            </script>
        </body>


        </html>
@stop
