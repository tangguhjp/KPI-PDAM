<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>KPI PDAM Surya Sembada Kota Surabaya</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap-datepicker.css')}}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Font Awesome JS -->
    <script src="{{asset('js/jquery/jquery.js')}}"></script>
    <script defer src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap/bootstrap-datepicker.js')}}"></script>
    <script defer src="{{asset('js/solid.js')}}"></script>
    <script defer src="{{asset('js/fontawesome.js')}}"></script>
    <script defer src="{{asset('js/jquery-3.1.0.min.js')}}"></script>
    <script defer src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.min.css')}}">
</head>

<body>
        <!-- Sidebar  -->
        <?php $role = Auth::user()->role; ?>

        <div class="wrapper">
            <nav id="sidebar" class="active">
                <div class="sidebar-header">
                    <img src="{{asset('img/logo.png')}}" alt="logo_PDAM" width="224" height="65">
                </div>

                <ul class="list-unstyled components">
                    <li>
                        <a href="{{route('home')}}">Home</a>
                    </li>
                    <li class="active">
                        <a href="#KPISubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">KPI</a>
                        <ul class="collapse list-unstyled" id="KPISubmenu">
                            {{--@if($role != 4)--}}
                            <li>
                                {{--tugas user--}}
                                <a href="{{route('responsibility')}}">Penanggung Jawab KPI</a>

                            </li>
                                {{--@endif--}}
                                @if($role != 1)
                            <li>
                                {{--tugas bawahan--}}
                                <a href="{{route('approve')}}">Sebagai atasan Penanggung Jawab KPI</a>
                            </li>
                                @endif
                        </ul>
                    </li>
                    @if($role != 0)
                    <li>
                        <a href="#masterSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Master</a>
                        <ul class="collapse list-unstyled" id="masterSubmenu">
                            <li>
                                <a href="{{route('input-program')}}">Masukkan Program</a>
                            </li>
                            <li>
                                <a href="#">Master 2</a>
                            </li>
                            <li>
                                <a href="#">Master 3</a>
                            </li>
                        </ul>
                    </li>
                        @endif
                </ul>
            </nav>
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid">

                        <button type="button" id="sidebarCollapse" class="btn btn-light sidebar-btn">
                            <i class="fas fa-align-justify"></i>
                        </button>

                        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fas fa-align-justify"></i>
                        </button>
                        <h4>{{Auth::user()->name}}</h4>
                        <div align="right">

                            <button class="btn btn-light " type="button" ><a href="/auth/logout">Logout<i class="fas fa-sign-out-alt"></i></a></button>
                        </div>
                    </div>
                </nav>
                @yield('content')
            </div>
        </div>

        <!--Modal: Login / Register Form-->
        <div class="modal fade" id="modalLRForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog cascading-modal" role="document">
                <!--Content-->
                <div class="modal-content">

                    <!--Modal cascading tabs-->
                    <div class="modal-c-tabs">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-2 light-blue darken-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab">
                                    <!--                                        <i class="fa fa-user mr-1" style="visibility: hidden;"></i>-->
                                    Realisasi
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel8" role="tab">
                                    CPAR
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel9" role="tab">
                                    Target
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#panel10" role="tab">
                                    Ditolak KKP
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panels -->
                        <div class="tab-content">
                            <!--Panel 7-->
                            <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                                <!--Body-->
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <h5>Realisasi Bulan <label href="" id="bulan"></label></h5>
                                        <label>Realisasi </label>
                                        <div class="text-left mt-2">
                                            <form id="upload-file" action="/testname" method="POST">
                                                {!! csrf_field() !!}
                                                <div class="form-group">
                                                </div>
                                                <div class="text-left mt-2">
                                                </div>
                                                <div class="text-center mt-2">
                                                    <input type="hidden" name="month" id="month" class="form-control">
                                                    <input type="hidden" name="programId" id="programId" class="form-control" />
                                                    <div class="text-left mt-2">
                                                        <input type="file" name="file" id="fileInput">
                                                    </div>
                                                    <input type="submit" class="btn btn-primary" id="btnSave" value="Simpan"></input>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.Panel 7-->

                            <!--Panel 8-->
                            <div class="tab-pane fade" id="panel8" role="tabpanel">
                                <!--Body-->
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <label>Status : </label>
                                        <p></p>
                                        <label><b>Alasan jika DITOLAK</b></label>
                                        <input type="text" id="modalLRInput10" class="form-control form-control-sm validate">
                                        <div class="text-center mt-2">
                                            <button class="btn btn-success">Terima</button>
                                            <button class="btn btn-danger">Tolak</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.Panel 8-->

                            <!--Panel 9-->
                            <div class="tab-pane fade" id="panel9" role="tabpanel">
                                <!--Body-->
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <table class="md-form form-sm mb-5">
                                            <tr>
                                                <th>Triwulan Ke-</th>
                                                <th>Target</th>
                                            </tr>
                                            <tr>
                                                <td>Triwulan Ke-1</td>
                                                <td>-%</td>
                                            </tr>
                                            <tr>
                                                <td>Triwulan Ke-2</td>
                                                <td>-%</td>
                                            </tr>
                                            <tr>
                                                <td>Triwulan Ke-3</td>
                                                <td>-%</td>
                                            </tr>
                                            <tr>
                                                <td>Triwulan Ke-4</td>
                                                <td>-%</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--Panel 10-->
                            <div class="tab-pane fade" id="panel10" role="tabpanel">
                                <!--Body-->
                                <div class="modal-body mb-1">
                                    <div class="md-form form-sm mb-5">
                                        <label>Ditolak KKP</label>
                                        <table class="md-form form-sm mb-5">
                                            <tr>
                                                <th>Alasan</th>
                                                <th>Tanggal</th>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>

    <!--Modal: Login / Register Form-->
    
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
    <!-- Popper.JS -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>

    <script src="js/placeholders.min.js"></script> <!-- polyfill for the HTML5 placeholder attribute -->
    <script src="js/main.js"></script> <!-- Resource JavaScript -->
    <script src="{{asset('js/modal.js')}}"></script>


</body>

</html>