<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="token" content="{{ csrf_token() }}">


    <title>KPI PDAM Surya Sembada Kota Surabaya</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Font Awesome JS -->
    <script defer src="{{asset('js/solid.js')}}"></script>
    <script defer src="{{asset('js/fontawesome.js')}}"></script>
    <script defer src="{{asset('js/jquery-3.1.0.min.js')}}"></script>
    <script defer src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.min.css')}}">
</head>

<body>
        <!-- Sidebar  -->
        <div class="wrapper">
            <div class="modal-content" style="min-height: 500px!important;">
                <!--Modal cascading tabs-->
                <div class="modal-c-tabs">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-2 light-blue darken-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab">
                                <!--                                        <i class="fa fa-user mr-1" style="visibility: hidden;"></i>-->
                                Realisasi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel8" role="tab">
                                CPAR
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel9" role="tab">
                                Target
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#panel10" role="tab">
                                Ditolak KKP
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--Panel 7-->
                        <div class="tab-pane fade in show active" id="panel7" role="tabpanel">
                            <!--Body-->
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-5">
                                    <h5>Realisasi Bulan :<label href="" id="bulan"></label> {{$data['label']}} </h5>
                                    {{--<h4>Realisasi </h4>--}}
                                    <div class="text-left mt-2">
                                        <form id="upload-file" action="/inputFile" method="POST" enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                                <label class="font-weight-bold" >File  :  </label>
                                                @if($data['programId'][$data['month']]['path'] == null)
                                                    <label for="">-</label>
                                                @elseif(Auth::user()->id != $data['programId']->responsible)
                                                    <label for="">jufsfj.excel</label>
                                                    <a class="btn btn-success" href="#" role="button">Download</a>
                                                @endif

                                                @if(Auth::user()->id == $data['programId']->responsible && $data['programId'][$data['month']]['path'] == null)
                                                <div class="mt-2">
                                                    <label for="fileInput">Realisasi : </label>
                                                    <input type="hidden" name="label" id="label" class="form-control" value="{{$data['label']}}">
                                                    <input type="hidden" name="month" id="month" class="form-control" value="{{$data['month']}}">
                                                    <input type="hidden" name="programId" id="programId" class="form-control" value="{{$data['programId']->id}}"/>
                                                    <input type="file" name="fileExcel" id="fileInput">
                                                    <div class="text-center">
                                                        <button id="btnSave" class="btn btn-primary text-center">Submit</button>
                                                        {{--<input type="submit" class="btn btn-primary text-center" id="btnSave">--}}
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.Panel 7-->

                        <!--Panel 8-->
                        <div class="tab-pane fade" id="panel8" role="tabpanel">
                            <!--Body-->
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-5">
                                    <label>Status : </label>
                                    <p></p>
                                    <label><b>Alasan jika DITOLAK</b></label>
                                    <textarea name="reason" rows="5" cols="30"></textarea>
                                    <form id="upload-file" action="/inputFile" method="POST" enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="mt-2">
                                                <input type="hidden" name="label" id="label" class="form-control" value="{{$data['label']}}">
                                                <input type="hidden" name="month" id="month" class="form-control" value="{{$data['month']}}">
                                                <input type="hidden" name="programId" id="programId" class="form-control" value="{{$data['programId']->id}}"/>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="text-center">
                                        <input type="submit" class="btn btn-primary text-center" id="btnSave">
                                        <button id="accept" class="btn btn-success">Terima</button>
                                        <button class="btn btn-danger">Tolak</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.Panel 8-->

                        <!--Panel 9-->
                        <div class="tab-pane fade" id="panel9" role="tabpanel">
                            <!--Body-->
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-5">
                                    <table class="md-form form-sm mb-5">
                                        <tr>
                                            <th>Triwulan Ke-</th>
                                            <th>Target</th>
                                        </tr>
                                        <tr>
                                            <td>Triwulan Ke-1</td>
                                            <td>-%</td>
                                        </tr>
                                        <tr>
                                            <td>Triwulan Ke-2</td>
                                            <td>-%</td>
                                        </tr>
                                        <tr>
                                            <td>Triwulan Ke-3</td>
                                            <td>-%</td>
                                        </tr>
                                        <tr>
                                            <td>Triwulan Ke-4</td>
                                            <td>-%</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--Panel 10-->
                        <div class="tab-pane fade" id="panel10" role="tabpanel">
                            <!--Body-->
                            <div class="modal-body mb-1">
                                <div class="md-form form-sm mb-5">
                                    <label>Ditolak KKP</label>
                                    <table class="md-form form-sm mb-5">
                                        <tr>
                                            <th>Alasan</th>
                                            <th>Tanggal</th>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
    <!-- Popper.JS -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    //close pop up windows and refresh parent
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('#btnSave').click(function () {--}}
                {{--window.opener.location.reload(true);--}}
                {{--window.close();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

    <script>
    $(document).ready(function () {
        $('#accept').click(function () {
            var form = document.createElement('form');
            var params = {
                "_token": "{{ csrf_token() }}",
                'programId' : "{{$data['programId']->id}}",
                'month' : "{{$data['month']}}" ,
                'label':"{{$data['label']}}"};
            form.setAttribute("method", "post");
            form.setAttribute("action", "accept");
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            form.submit();
            window.opener.location.reload(true);
        });

        $('#btnSave').click(function () {
            document.getElementById("upload-file").submit();
            window.opener.location.reload(true);
        });
    });
    </script>
    <script src="{{asset('js/modal.js')}}"></script>


</body>

</html>