@extends('menu.menu-master')
@section('content')

        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            {{--<script src="{{asset('js/jquery.min.js')}}"></script>--}}
            {{--<script src="{{asset('js/jquery-3.3.1.js')}}"></script>--}}
            <script defer src="{{asset('js/dataTables.fixedColumns.min.js')}}"></script>
        </head>
        <body>
        <?php $responsible = Auth::user()->name; ?>

        <div id="content">
                <div class="modal hide" id="addBookDialog">
                    <div class="modal-header">
                        <button class="close" data-dismiss="modal">×</button>
                        <h3>Modal header</h3>
                    </div>
                    <div class="modal-body">
                        <p>some content</p>
                    </div>
                </div>
                <div>
                    <div>
                        <h2>Master KPI</h2>
                        <table id="tabel1" class="display row-border order-column" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Jabatan</th>
                                    <th class="fixed-side">Indikator<a href="" style="visibility: hidden">aaaaaaaa</a></th>
                                    <th class="fixed-side">Pengukuran</th>
                                    <th>Satuan</th>
                                    <th>Target</th>
                                    <th>Periode</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>Mei</th>
                                    <th>Juni</th>
                                    <th>Juli</th>
                                    <th>Agu</th>
                                    <th>Sept</th>
                                    <th>Okt</th>
                                    <th>Nov</th>
                                    <th>Des</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $data1)
                                {{ csrf_field() }}
                                <tr>
                                    <input type="hidden" value="{{$data1->responsible}}" id="responsible">
                                    <td>{{$data1->responsible}}</td>
                                    <td>{{$data1->indicato}}</td>
                                    <td>{{$data1->measurement}}</td>
                                    <td class="text-center">{{$data1->unit}}</td>
                                    <td class="text-center">{{$data1->target}}</td>
                                    <td class="text-center">{{$data1->period}}</td>

                                    <?php
                                        $monthId = ['jan','feb','mar','apr','may','june','july','aug','sept','oct', 'nov', 'dec'];
                                        $monthName = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus',
                                            'September','Oktober', 'November', 'Desember'];
                                    ?>
                                    @for($i = 0; $i<12;$i++)
                                        <?php
                                            if (@$data1[$monthId[$i]]['target']==null){
                                                $target = null;
                                            }else{
                                                $target = $data1[$monthId[$i]]['target'];
                                            }
                                        $approval = $data1[$monthId[$i]]['approved'];
                                        $targetValue = $target;
                                        $warna = '';
                                        if ($target == null){
                                            $targetValue = '?';
                                            $warna = 'default-cell';
                                        }elseif ($approval == 2){
                                            $warna = 'green-cell';
                                        }elseif($approval == 1){
                                            $warna = 'bg-primary';
                                        }elseif ($approval == 0){
                                            $warna = 'yellow-cell';
                                        }
                                        ?>
                                        <td class="{{$warna}} text-center"><a class="open-Dialog" href=""
                                               data-id="{{$data1->id}}"
                                               data-month="{{$monthName[$i]}}"
                                               id="{{$monthId[$i]}}">{{$targetValue}}</a></td>
                                    @endfor
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $("#tabel1").DataTable({
                        "scrollY": true,
                        "scrollX": true,
                        "scrollCollapse": true,
                        "language": {
                            "decimal":        "",
                            "emptyTable":     "Tidak ada data yang tersedia di tabel",
                            "info":           "Menampilkan _START_ sampai _END_ dari _TOTAL_ masukan",
                            "infoEmpty":      "Menampilkan 0 sampai 0 dari 0 masukan",
                            "infoFiltered":   "(difilter dari _MAX_ total masukan)",
                            "infoPostFix":    "",
                            "thousands":      ".",
                            "lengthMenu":     "Menampilkan _MENU_ masukan",
                            "loadingRecords": "memuat...",
                            "processing":     "Sedang di proses...",
                            "search":         "Pencarian:",
                            "zeroRecords":    "Arsip tidak ditemukan",
                            "paginate": {
                                "first":      "Pertama",
                                "last":       "Terakhir",
                                "next":       "lanjut",
                                "previous":   "kembali"
                            },
                            "aria": {
                                "sortAscending":  ": aktifkan urutan kolom ascending",
                                "sortDescending": ": aktifkan urutan kolom descending"
                            }
                        },
                        "fixedColumns":   {
                            "leftColumns": 6,
                        },
                        "oSearch": {
                            "bSmart": false,
                            "bRegex": true,
                            "sSearch": ""
                        }
                    });

                });
            </script>
            <script>
                $(document).on("click", ".open-Dialog", function () {
                    //Pop up windows
                    var winName='MyWindow';
                    var winURL='/popUpInput';
                    var windowoption='resizable=no,height=500,width=500,location=0,menubar=0,scrollbars=1';
                    var params = {
                        'programId' : $(this).data('id'),
                        'month' :this.id ,
                        'label':$(this).data('month')};
                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", winURL);
                    form.setAttribute("target",winName);
                    for (var i in params) {
                        if (params.hasOwnProperty(i)) {
                            var input = document.createElement('input');
                            input.type = 'hidden';
                            input.name = i;
                            input.value = params[i];
                            form.appendChild(input);
                        }
                    }

                    document.body.appendChild(form);
                    window.open('', winName,windowoption);
                    form.target = winName;
                    form.submit();
                    document.body.removeChild(form);
                    return false;
                });
            </script>
            <script>
                // setInterval("my_function();",1000);

                function my_function(){
                    window.location = location.href;
                }
            </script>
        </body>
        </html>
@stop
